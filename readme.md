SPRING FRAMEWORK - ZÁKLADNÍ KURZ
================================

*Autor*: Ing. Jan Kolomaznik Ph.D.
*Email*: jan.kolomaznik@gmail.com
*mobil*: +420 732 568 669


V rámci základního školení se seznámíte s možnostmi vývoje aplikací v prostředí Spring Framework. 
Kurz Vás dále seznámí s možnostmi využití servletu, JavaServer Pages (JSP), Spring MVC, práci s databází, apod.

Osnova kurzu 
------------
- **[Úvod do Spring Framework](/spring)**
  - Architektura Spring Framework
  - Spring & Java Beans
  - Konfigurace aplikace a metadat
- **[Spring Boot](/spring.boot) - samostatné aplikace**
- **[Dependency injection](/spring.di)**
- **[Spring MVC](/spring.boot.mvc)**
  - Definice
  - Vztah k standardní Javě
  - Vytvoření základní MVC aplikace
  - Vytvoření základního kontroleru a pohledu v JSP
- **[View technologie](/spring.boot.mvc-thymeleaf) (Thymeleaf)**
  - Jednoduchá strana
  - Knihovna značek
- **[Servlet](/spring.jee)**
  - Definice a vytvoření
  - Generování výstupu
  - Zpracování vstupu a výstupu (jednoduchá formulářová aplikace)
- **Spring Rest**
  - [RESTful Web Services](/spring.boot.rest-service)
  - [Hypermedia REST](/spring.boot.rest-hypermedia)
- **Přístup k datům a integrace s MVC**
  - Spring Data
  - JDBC (bez hibernate)
  - Data a REST
- **[Aspektově-orientované programování (AOP) v Springu](/spring.boot.aop)**
- **[Konfigurace aplikace](/spring.boot.config)**
