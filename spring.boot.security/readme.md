[remark]:<class>(center, middle)
# Spring Boot
*Spring Security Architecture*

[remark]:<slide>(new)
## Úvod
Bezpečnost aplikací se skládá ze dvou více či méně nezávislé kroků

- **Autenttizace**: kdo jste?
- **Autorizace**: co máte povoleno dělat?). 

Někdy lidé říkají "kontrola přístupu" namísto "autorizace"

Ve spring se obě části konfigují nezávisle na různých místech aplikace.

Architektura Spring Security frameworku pracuje s ordělením techto dvou kroků.

[remark]:<slide>(new)
### Authentication

Hlavním rozhraním pro ověřování uživatle je `AuthenticationManager`

Toto rozhraní ma jednu metodu:

```java
public interface AuthenticationManager {

  Authentication authenticate(Authentication authentication)
    throws AuthenticationException;

}
```

`AuthenticationManager` provádí jednu z 3 věcí v metodě `authenticate()`:

1. vrátí `Authentication` (normálně s ověřeným = true), pokud může ověřit, že uživatel je skutečně tím, za koho se vydává.

2. vyvolat výjimku `AuthenticationException` pokud uživatel není tím za koho se vydábá.
 
3. vrátí se `null`, pokud se nemůže rozhodnout.

[remark]:<slide>(new)
#### AuthenticationException
AuthenticationException je běhová výjimka. 

Obvykle se s aplikací tuto výjimku nezachytáváme.

Například webové uživatelské rozhraní vykreslí stránku, která říká, že ověřování selhalo a zpětná služba HTTP odešle odpověď **401** s hlavičkou `WWW-Authenticate` nebo bez ní v závislosti na kontextu.

[remark]:<slide>(wait)
#### ProviderManager
Nejčastěji používanou implementací `AuthenticationManager` je `ProviderManager`.

Ten interně využívá návrhový vzor **Chain of responsibility**.

Obsahuje navíc metodu `supports`, která umožňuje volajícímu dotaz, zda implementace podporuje daný typ autorizace.

```java
public interface AuthenticationProvider {

	Authentication authenticate(Authentication authentication)
			throws AuthenticationException;

	boolean supports(Class<?> authentication);

}
```

[remark]:<slide>(new)
#### ProviderManager chain
`ProviderManager` může podporovat několik různých autentizačních mechanismů ve stejné aplikaci delegováním do řetězce `AuthenticationProviders`. 

Pokud `ProviderManager` nerozpozná určitý typ instance autentizace, bude přeskočen.

`ProviderManager` má volitelného rodiče, který může konzultovat. 
- Pokud se všichni poskytovatelé vrátí `null`. 
- Není-li rodič k dispozici, výsledkem ověřování `null` je `AuthenticationException`.

[remark]:<slide>(new)
#### Seskupování zdrojů v aplikaci
Někdy aplikace obsahuje logické skupiny chráněných zdrojů
 - např. Všechny webové zdroje, které odpovídají vzoru cesty `/api/**`
 
Každá skupina může mít vlastní vyhrazený `AuthenticationManager`. 
- Často je každý z nich `ProviderManager` a sdílí rodiče. 
- Rodič je pak druhem "globálního" auentizačního mechanizmu.

![](media/authentication.png)

[remark]:<slide>(new)
### Přizpůsobení Authentication Managers
Spring Security poskytuje pomocné třídy pro rychlou konfiguraci.
 
Nejčastěji používanou je `AuthenticationManagerBuilder`.

Ten umožňuje pracovat s uživateleli v paměti, JDBC nebo LDAP nebo pro přidání vlastní UserDetailsService. 

Zde je příklad aplikace konfigurovající globální (nadřazenou) autentizační správu:

```java
@Configuration
public class ApplicationSecurity extends WebSecurityConfigurerAdapter {

   ... // web stuff here

  @Autowired
  public initialize(AuthenticationManagerBuilder builder, DataSource dataSource) {
    builder
        .jdbcAuthentication()
        .dataSource(dataSource)
        .withUser("dave").password("secret").roles("USER");
  }

}
```

[remark]:<slide>(new)
#### Lokální Authentication Managers
Tento příklad se týká webové aplikace s použitím `AuthenticationManagerBuilder`
  
Všimněte si, že `AuthenticationManagerBuilder` je `@Autowired` do metody v `@Bean`.
 
Takto vytvoříme globální nastavení `AuthenticationManager`

Naopak, kdybychom to udělali takto:
```java
@Configuration
public class ApplicationSecurity extends WebSecurityConfigurerAdapter {

  @Autowired
  DataSource dataSource;

   ... // web stuff here

  @Override
  public configure(AuthenticationManagerBuilder builder) {
    builder
        .jdbcAuthentication()
        .dataSource(dataSource)
        .withUser("dave").password("secret").roles("USER");
  }

}
```

[remark]:<slide>(new)
#### Rozdíly mezi lokálním a globálním nastavení
Přetížení metody `configure`.
 
Ppak je `AuthenticationManagerBuilder` používán pouze k vytvoření "lokálního" autentizačního správce.

Spring Boot poskytuje výchozí globální `AuthenticationManager`
 
Výchozí nastavení je dostatečně zabezpečené samo o sobě, nemusíte jej měnit.

Pokud provedete libovolnou konfiguraci, která vytváří službu AuthenticationManager, můžete ji často provést místně na prostředky, které chráníte a nemusíte se starat o globální výchozí nastavení.

[remark]:<slide>(new)
### Authorization or Access Control
Jakmile je ověřování úspěšné, můžeme přejít na autorizaci.
 
O tu se stará `AccessDecisionManager`. 

Existují tři implementace poskytnuté framevorkem.
- Všechny tři jsou opět integrované pomocí návarhového vzoru **Chain of responsibility** řetězce `AccessDecisionVoter`
- Stejný mechanizmuss jako u `ProviderManager`, který deleguje na `AuthenticationProviders`.

`AccessDecisionVoter` vyhodnocuje `Authentication` a zabezpečená objek (reprezentující hlavního) a bezpečný objekt s kolekcí `ConfigAttributes`:

```java
boolean supports(ConfigAttribute attribute);

boolean supports(Class<?> clazz);

int vote(Authentication authentication, S object,
        Collection<ConfigAttribute> attributes);
```

[remark]:<slide>(new)
### AccessDecisionVoter a proces Authorization
Parametr `S objekt` je generický určen v definici `AccessDecisionManager`
 
`AccessDecisionVoter` - představuje všechno, k čemu by uživatel chtěl mít přístup:
- webový prostředek
- metoda v třídě Java

[remark]:<slide>(wait) 
`ConfigAttributes ` jsou také velmi obecné:
- představuje atributy pro zabezpečení objektu.
- určují úroveň oprávnění k přístupu k objektu. 

[remark]:<slide>(wait)
`ConfigAttribute` je rozhraní, ale má pouze jednu metodu
- ta je poměrně obecná a vrací řetězec
- takže tyto řetězce nějakým způsobem zakódují záměr vlastníka zdroje
- vyjadřují pravidla o tom, kdo má k němu přístup. 

[remark]:<slide>(wait)
Typický `ConfigAttribute` je:
- název role uživatele (**ROLE_ADMIN** nebo **ROLE_AUDIT**)
- často mají speciální formáty (například předponu **ROLE_**)
- nebo reprezentují výrazy, které je třeba vyhodnotit.

[remark]:<slide>(new)
#### Výchozí AccessDecisionManager
Většinou používáme pouze výchozí `AccessDecisionManager`, ten je založen na zásadě `AffirmativeBased`
 
Jakékoli přizpůsobení se obvykle projevuje u voličů, a to buď přidáním nových nebo úpravou způsobu, jakým fungují stávající.

[remark]:<slide>(wait)
Je velmi běžné používat konfigurační výrazy `ConfigAttributes`, které jsou výrazy Expression Expression Language (SpEL)
- například `isFullyAuthenticated() && hasRole('FOO')`. 

To je podporováno aplikací `AccessDecisionVoter`, která zpracovává výrazy a vytváří pro ně kontext. 

[remark]:<slide>(wait)
Rozšíření rozsahu výrazů, které lze zpracovat vyžaduje vlastní implementaci `SecurityExpressionRoot` a někdy také `SecurityExpressionHandler`.

[remark]:<slide>(new)
## Web Security
Spring Security ve vrstvě webových stránek je založeno na servletových filtrech.

Na následujícím obrázku je znázorněno typické vrstvení zpracovatelů pro jeden požadavek HTTP.

![](media/filters.png)

[remark]:<slide>(new)
## Filter chain
Spring Security je nainstalováno jako jediný filtr v řetězci.
 
Jeho typ concerete je ale `FilterChainProxy`.

V aplikaci Spring Boot je bezpečnostní filtr @Bean v ApplicationContext a je standardně nainstalován tak, aby byl aplikován na každou žádost.
 
Je nainstalován na pozici definovanou službou `SecurityProperties.DEFAULT_FILTER_ORDER`.

Ta je zase ukotvena v souboru `FilterRegistrationBean.REQUEST_WRAPPER_FILTER_MAX_ORDER`
 
Z pohledu kontejneru Spring Security je jediný filtr, ale uvnitř jsou další filtry, z nichž každý hraje zvláštní roli.

[remark]:<slide>(new)
#### Spring security filters
![](media/security-filters.png)

[remark]:<slide>(new)
### Detailnější pohled
Ve skutečnosti je v bezpečnostním filtru dokonce ještě jedna vrstva, obvykle je instalována jako `DelegatingFilterProxy`

Proxy deleguje na `FilterChainProxy`, který je vždy `@Bean`, obvykle s pevným jménem `springSecurityFilterChain`. 

Jedná se o filtr `FilterChainProxy`, který obsahuje celou bezpečnostní logiku uspořádanou uvnitř jako řetězec (nebo řetězy) filtrů. 

Všechny filtry mají stejné API (všechny implementují rozhraní filtru ze Servlet Spec) a všichni mají možnost vetovat zbytek řetězce.

K dispozici mohou být více filtračních řetězců, které spravuje Spring Security na stejné úrovni filtru `FilterChainProxy` a všechny jsou pro kontejner neznámé. 

Filtr obsahuje seznam filtrovacích řetězců a odesílá požadavek na první řetězec, který odpovídá. 

Na následujícím obrázku je zobrazeno odeslání na základě shody cesty požadavku (`/foo/**` odpovídá před `/**`). 

Nejdůležitějším znakem tohoto procesu odeslání je, že pouze jeden řetězec zpracovává žádost.

[remark]:<slide>(new)
#### Vnitřní členení filtrů
![](media/security-filters-dispatch.png)

[remark]:<slide>(new)
### Creating and Customizing Filter Chains
Výchozí řetězec filtru v Spring Boot app (pro `/**`) má předdefinovanou řadu SecurityProperties.BASIC_AUTH_ORDER. 

Můžete jej úplně vypnout nastavením parametru `security.basic.enabled = false`

Chcete-li jednoduše přidat `@Bean` typu `WebSecurityConfigurerAdapter` (nebo `WebSecurityConfigurer`)

```java
@Configuration
@Order(SecurityProperties.BASIC_AUTH_ORDER - 10)
public class ApplicationConfigurerAdapter extends WebSecurityConfigurerAdapter {
  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.antMatcher("/foo/**")
     ...;
  }
}
```

[remark]:<slide>(new)
#### WebSecurityConfigurerAdapter
Mnoho aplikací má zcela odlišné přístupové pravidlo pro jednu sadu zdrojů ve srovnání s jinou. 

Například aplikace, která je hostitelem uživatelského rozhraní a API zálohování, může podporovat autentizaci založenou na cookie přesměrováním na přihlašovací stránku pro součásti uživatelského rozhraní a autentizaci založenou na tokenu s odpovědí 401 na neověřené žádosti o součásti rozhraní API. 

Každá sada zdrojů má svůj vlastní WebSecurityConfigurerAdapter s jedinečnou objednávkou a vlastním požadavkem na porovnávání. 

Pokud se odpovídající pravidla překrývají, vyhraje nejstarší uspořádaný řetězec filtru.

[remark]:<slide>(new)
### Request Matching for Dispatch and Authorization
Řetěz bezpečnostního filtru má modul požadavku, který se používá k rozhodování, zda se má použít na požadavek HTTP. 

Jakmile je rozhodnuto o použití určitého řetězce filtrů, nejsou použity žádné další. 

Ovšem ve filtrovacím řetězci můžete mít mnohem jemnější kontrolu nad autorizací tím, že nastavíte další parametry v konfiguraci `HttpSecurity`.

```java
@Configuration
@Order(SecurityProperties.BASIC_AUTH_ORDER - 10)
public class ApplicationConfigurerAdapter extends WebSecurityConfigurerAdapter {
  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.antMatcher("/foo/**")
      .authorizeRequests()
        .antMatchers("/foo/bar").hasRole("BAR")
        .antMatchers("/foo/spam").hasRole("SPAM")
        .anyRequest().isAuthenticated();
  }
}
```

[remark]:<slide>(new)
## Method Security
Stejně jako podpora pro zabezpečení webových aplikací, Spring Security nabízí podporu pro uplatňování pravidel přístupu k Java metodám. 

Pro Spring Security je to jen jiný typ "chráněného zdroje". 

Pro uživatele to znamená, že pravidla přístupu jsou deklarována pomocí stejného formátu řetězců `ConfigAttribute` (např. Rolí nebo výrazů), ale na jiném místě ve vašem kódu. 

Prvním krokem je umožnit zabezpečení metod, například v konfiguraci nejvyšší úrovně naší aplikace:

```java
@SpringBootApplication
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SampleSecureApplication {
}
```

Následně můžeme definovat přístupová práva u metod:
```java
@Service
public class MyService {

  @Secured("ROLE_USER")
  public String secure() {
    return "Hello Security";
  }

}
```

Tento vzorek je služba se zabezpečenou metodou. 

Pokud jaro vytvoří `@Bean` tohoto typu, pak bude proxy a volající budou muset projít bezpečnostním stíhačem předtím, než je metoda skutečně provedena. 

Je-li přístup odepřen, volající získá namísto skutečného výsledku metody AccessDeniedException.

Existují další poznámky, které lze použít na metody pro vynucení bezpečnostních omezení, zejména `@PreAuthorize` a `@PostAuthorize`, které umožňují psát výrazy obsahující odkazy na parametry metody a návratové hodnoty.