[remark]:<class>(center, middle)
# Spring Framework

[remark]:<slide>(new)
## Úvod
Spring je jeden z nejrozšířenějších frameworků ve světe enterprise java. 

Jeho možnosti se rozrostly do obřích rozměrů a obsáhnout všechny jeho části už snad není ani v lidských silách. 

Spring boot je projekt, který využití Spring framework a navazujících knihoven usnadňuje a urychluje. 

Přináší princip convention over configuration a slouží k extrémně rychlému vyrábění aplikací postavených na springu. 

Spring cloud pak do tohoto světa připojuje mnoho moderních a populárních technologií ze světa microservices. 

[remark]:<slide>(new)
## Spring vs JavaEE(EJB)

Je alternativou k JavaEE, ale je jednodušší na použití. 

Není nutné využít všechno co nabízí najednou, lze si vybírat pouze požadovanou funkcionalitu. 

Také na rozdíl od JavaEE nevynucuje závislost na konkrétních třídách.

Je silně modulárni

Vychází z **IoC** a **Dependici injection** frameworku

Aktální verze: **5.0.7**

Dokumence dostulná na stránkách: [link](https://docs.spring.io/spring/docs/current/spring-framework-reference/index.html)

[remark]:<slide>(new)
### Spring Runtime moduly

![](media/Spring-overview.png)

[remark]:<slide>(new)
## Spring: základní pojmy
Když již známe základní pojmy, pojďme se podívat, jak je využívá Spring. 

Zde je nutné znát dva pojmy:

[remark]:<slide>(wait)
#### Bean
Objekt, který vykonává nějakou funkčnost (např. přidává data do databáze, vyhledává...). Beany žijí v kontejneru po celý běh aplikace. Lze s ním pracovat v celé aplikaci. Existují dva typy bean (scope).

- **Singleton** objekt je v celé aplikaci jen jednou. 
  Pokaždé, pokud si řekneme o daný objekt aplikačnímu kontextu, dostaneme stejnou instanci.
- **Prototype** je podobný jako singleton. 
  Rozdíl je v tom, že pokud si řekneme o daný objekt aplikačnímu kontextu, dostaneme vždy novou instanci.

[remark]:<slide>(wait)
#### Kontejner
V kontejneru, neboli aplikačním kontextu, žijí objekty (BEAN), které tvoří funkční jádro vaší aplikace. Kontejner se zavádí při startu aplikace a reprezentuje ho třída ApplicationCon­text. V celé aplikaci je jen jeden a dá se injektovat odkudkoli.

[remark]:<slide>(new)
## Konfigurace aplikačního kontextu
Konfigurovat kontext můžeme pomocí XML souboru nebo pomocí Java class. 

Obě konfigurace si funkčně odpovídají. 

Preferovaná cesta je Java class.

[remark]:<slide>(new)
### Java konfigurace
Je realizována obyčejnou Java třídou, ve které jsou použity anotace pro tvorbu aplikačního kontextu. 

Je dobré, si pro konfigurační třídy udělat speciální package (configuration) nebo je umistit do "základního" balíčku.

- **`@Configuration`** vytvoří z dané třídy konfigurační třídu
- **`@Import`** spojí dvě konfigurace (naimportuje jinou konfiguraci)
- **`@Bean`** vytvoří beanu; typ je návratová hodnota a název je název metody (pokud se nepoužije name). 
  Lze i změnit defaultní singleton scope (`scope=DefaultSco­pes.PROTOTYPE`)
- **`@Autowired`** injektuje instanci jiné beany
- **`@ComponentScan`** - proskenuje zadané package. 
  Pokud narazí na speciální anotace:
  - `@Controller`: prezentační vrstva; 
  - `@Service`: aplikační vrstva; 
  - `@Repository`: datová vrstva) vytvoří z daných tříd beany. 
  - *Užitečná věc pro rychlou tvorbu bean.*

[remark]:<slide>(new)
#### Příklad Java konfigurace
```java
// jedná se o konfiguraci
@Configuration
// naimportuje konfiguraci z třídy StorageConfig
@Import({StorageConfig.class})
// skenuje cz.itnetwork a tvoří beany (@Component, @Service...)
@ComponentScan("cz.itnetwork")
public class ContextConfig {
        // vytvoří beanu typu CarDao a názvem carRepository
        @Bean(name="carRepository")
        public CarDao carDao() {
                return new CarDaoImpl();
        }

        // vytvoří beanu CarService a injektuje ji CarDao (CarRepository)
        @Bean
        @Autowired
        public CarService carService(CarDao carDao) {
                return new CarServiceImpl(carDao);
        }
}
```

[remark]:<slide>(new)
### XML konfigurace
Je reprezentována XML souborem. 

Konfigurace se musí nacházet v Resources a být na classpath.

Jedná se o starší způsob z "původniho".

Trendem je se tomuto způsobu konfigurace vyhnou, ale přesto se s ním čato setkáte.

*Doporučení*: naučit se přepsat xml do Java

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd">

    <bean id="..." class="...">   
        <!-- collaborators and configuration for this bean go here -->
    </bean>

    <bean id="..." class="...">
        <!-- collaborators and configuration for this bean go here -->
    </bean>

    <!-- more bean definitions go here -->

</beans>
```

[remark]:<slide>(new)
#### Použití kontejneru
Pro práci s aplikačním kontextem slouží beana `ApplicationContext`. 

Tato bean má metodu `getBean()`, pomocí níž získáte jakoukoli beanu z kontejneru.

```java
@Configuration
public class ContextConfig {
        @Bean
        public NameStrategy nameStrategy() {
                return new NameStrategyImpl();
        }
}

public class UpdateFactoryImpl implements UpdateFactory {
        // zisk pristupu ke kontejneru
        @Autowired
        private ApplicationContext applicationContext;

        public Strategy getStrategy(Change change) {
        if (change.isChangeName()) {
                // vytažení beany NameStrategy
                return applicationContext.getBean(NameStrategy.class);
        }
}
```

Příklad je výtažek kódu, kde je využit návrhový vzor Factory. Podle změny (change) se rozhoduje kterou strategii má factory vytvořit (vytáhnout z aplikačního kontextu). V našem případě se jedná o NameStrategy.

[remark]:<slide>(new)
## Základní rozdělení modulů 

- **[Core](https://docs.spring.io/spring/docs/current/spring-framework-reference/core.html#spring-core)**	
  IoC container, Events, Resources, i18n, Validation, Data Binding, Type Conversion, SpEL, AOP.

- **[Testing](https://docs.spring.io/spring/docs/current/spring-framework-reference/testing.html#testing)**	
  Mock objects, TestContext framework, Spring MVC Test, WebTestClient.

- **[Data Access](https://docs.spring.io/spring/docs/current/spring-framework-reference/data-access.html#spring-data-tier)**	
  Transactions, DAO support, JDBC, ORM, Marshalling XML.

- **[Web Servlet](https://docs.spring.io/spring/docs/current/spring-framework-reference/web.html#spring-web)**	
  Spring MVC, WebSocket, SockJS, STOMP messaging.

- **[Web Reactive](https://docs.spring.io/spring/docs/current/spring-framework-reference/web-reactive.html#spring-webflux)**	
  Spring WebFlux, WebClient, WebSocket.

- **[Integration](https://docs.spring.io/spring/docs/current/spring-framework-reference/integration.html#spring-integration)**	
  Remoting, JMS, JCA, JMX, Email, Tasks, Scheduling, Cache.

