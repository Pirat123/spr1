[remark]:<class>(center, middle)
# Spring Boot
*Basic MVC*

[remark]:<slide>(new)
## Úvod
V rámci toho cvičení vytvožíme jednoduchou MCV aplikaci obsahujicí:
- **statickou stránku**
  - statická stránka bude na adrese /index.html
  - bude obsahovat odkaz na dynamickou stránku
- **dynamickou stránku**
  - bude napojená na adrese: `http://localhost:8080/greeting`
  - vygeneruje html stránku se zprávou *Hello, World!* 
  - bude akceptovat parametr `name`
  
[remark]:<slide>(new)
## Gradle konfigurace
Pro konfiguraci projektu použijem [Gradle](https://gradle.org/) script. 

Podobně je ale možno použít i [Maven](https://maven.apache.org/).

Ve sriptu použijeme následující `dependencies`

#### `build.gradle`
```groovy
dependencies {
    compile("org.springframework.boot:spring-boot-starter-web")
    compile("org.springframework.boot:spring-boot-starter-thymeleaf")
    compile("org.springframework.boot:spring-boot-devtools")
}
```

[remark]:<slide>(new)
## Vytvoření MVC Controlleru
Ve Spring je každý webový požadavek zpracován Controllerem.
 
Tyto třídy jsou označeny anotací `@Controller`. 

Obsahují metody označené annotaci například `@GetMapping`, které odpovídají za zpracování požadavku.

Jejich úkolem je připravit `model` and `view`.
- **`view`**: název pohledu uloženého v souboru: `src/main/resources/templates/{name}.html`
  - může byt vraceno jako `String` nebo součát bojektu `ModelAndView`
- **`model`**: cytřejší mapa typu `<String, Object>` 

V následujícím příkladu `GreetingController`:
- je zpracovává požadavky `GET /greeting`
- `name` reprezentuje data pro šablonu
- navratová hodnota je názvem šablony.

[remark]:<slide>(new)
#### `src/main/java/hello/GreetingController.java`
```java
package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {

    @GetMapping("/greeting")
    public String greeting(
            @RequestParam(name="name", defaultValue="World") String name, 
            Model model) 
    {
        model.addAttribute("name", name);
        return "greeting";
    }

}
```

[remark]:<slide>(new)
## Vytvoření šablony
Pro implemntaci šablony použijem tchnologii [Tymeleaf](https://www.thymeleaf.org/).

Html stránky generujeme na straně serveru. 

Thymeleaf analyzuje šablonu greeting.html zpracuje značku `th:text`.
 
Za argument `${name}` doplní hodnotu definovanou v řadiči.

#### `src/main/resources/templates/greeting.html`
```html
<!DOCTYPE HTML>
<html xmlns:th="http://www.thymeleaf.org">
<head>
    <title>Getting Started: Serving Web Content</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
    <p th:text="'Hello, ' + ${name} + '!'" />
</body>
</html>
```

[remark]:<slide>(new)
## Usnadnění vývoje web aplikací
Spolešným rysem vývoje webových aplikací je restart aplikace i prohlížeče. 
Toto může být časově náročnou operací, proto Spring Boot poskytuje knihovnu `spring-boot-devtools`.

- Provádí hot swapping
- Zakazuje caching stránek
- Povoluje automatické obnovení prohlížeče LiveReload
- A další nastavení pro usnadnění vývoje

[remark]:<slide>(new)
## Vytvoření statické stránky
Statické zdroje (HTML, JavaScript, CSS, ...) se umisťují do složky `src/main/resources/static`.
 
Jako statický zdroj je možné považovat například strínku `index.html`.
 
Ta se zobrazuje v případě požadavku na `/`.
 
#### `src/main/resources/static/index.html`
```html
<!DOCTYPE HTML>
<html>
<head>
    <title>Getting Started: Serving Web Content</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
    <p>Get your greeting <a href="/greeting">here</a></p>
</body>
</html>
```
 
[remark]:<slide>(new)
## Vytvoření spustitelné aplikace
Pro spuštění aplikace můžeme vytvořit jednoduchou spouštěcí třídu s metodou `main`. 

#### `src/main/java/hello/Application.java`
```java
package hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
```

Pokud používáte Gradle, můžete spustit aplikaci pomocí `./gradlew bootRun`

[remark]:<slide>(new)
## Testování aplikace
Po zadání url `http://localhost:8080/greeting` do prohlížeče, měli by jste získat nasleující výstup:

```
Hello, World!
```

Pokud do uml nastavíme paramter `name`: `http://localhost:8080/greeting?name=User`, odpověď se změní následovně:

```
Hello, User!
```

[remark]:<slide>(new)
## Další zdroje

- https://www.mkyong.com/spring-boot/spring-boot-hello-world-example-thymeleaf/

