[remark]:<class>(center, middle)
# Spring Framework
*Java EE technologie*

[remark]:<slide>(new)
## Java Enterprise Edition
Java Enterprise Edition by se dalo přeložit jako podniková edice Javy. 

Název je poněkud zavádějící a to hned z několika důvodů:

[remark]:<slide>(wait)
1. Standardní edice Javy se samozřejmě používá v podnicích úplně stejně, jako JEE. Naopak JEE není nijak vázaná na komerční sféru a velmi jednoduše v ní můžeme naprogramovat např. osobní webový blog.

[remark]:<slide>(wait)
2. JEE není jiná verzi Javy, ale pouze sada knihoven do Javy SE (standardní edice). To je určitě dobrá zpráva, v JEE tedy budete programovat úplně stejně, jako v JSE. Oracle ve skutečnosti dodává pouze rozhraní a existuje hned několik implementací JEE.

[remark]:<slide>(wait)
3. JEE je zaměřena na tvorbu webových aplikací v Javě. Dle mého názoru by byl výstižnější název webová edice, ale s tím již nic nenaděláme.

[remark]:<slide>(wait)
Původní název vznikl proto, že JEE obsahuje spoustu hotových řešení, které lze použít pro rozsáhlé webové aplikace. 

[remark]:<slide>(new)
### Rozšířenot
V praxi je JEE asi nejrozšířenější podniková technologie, používá ji obrovské množství velkých firem a zahrnuje v sobě velké množství různých pokročilých technologií, ze kterých se výsledná aplikace skládá. 

Z předešlé věty by mělo vyjít najevo, že se jedná opravdu o robustní řešení, které uspokojí poptávku po náročných aplikacích jako velké státní registry, bankovní aplikace a podobně. 

Zkušený programátor v JEE se má dobře, na druhou stranu takový člověk musí něco umět.

[remark]:<slide>(new)
### Charakteristiky podnikových aplikací
Podnikovou aplikací se obvykle myslí aplikace, která:
- Obsluhuje velké množství uživatelů najednou
- Pracuje s velkým množstvím dat v databázi
- Komunikuje s dalšími systémy
- Je robustní a bezpečná

Java Enterprise Edition se snaží poskytnout co nejvíce standardních a kvalitních komponent, které usnadní tvorbu právě takových aplikací. 

Ctí tzv. třívrstvou architekturu, kde je aplikace rozdělena na databázovou vrstvu, vrstvu obchodní logiky a vrstvu prezentační.

[remark]:<slide>(new)
## Webové aplikace
JEE funguje stejně jako např. konkurenční ASP.NET nebo PHP na architektuře klient-server. 

Aplikace v JEE je tedy program, jehož výstupem je HTML stránka (REST, SOAP, ...). 

Java zde běží na straně serveru, což je rozdíl oproti JSE, kde naše Java aplikace běžela přímo na počítači klienta. 

JEE tedy běží na serveru, na základě požadavků od klienta vygeneruje odpověď a pošle ji klientovi. 
![](media/Spring-overview.png)

[remark]:<slide>(new)
## Aplikační servery
JEE běží na tzv. aplikačním serveru. 

To je software, který běží na serveru a který zpracovává HTTP/HTTPS požadavky klientů, spravuje spojení s databází v tzv. connection poolu, odesílá emaily a podobně. Právě jednotlivé aplikační servery poskytují implementaci rozhraní JEE. 

- **JBoss Application Server** - Asi nejznámější aplikační server, který je opensource
- **GlassFish** - Jednoduchý aplikační server od Oracle.
- **WebSphere Application Server** - Robustní komerční řešení pro velké aplikace od IBM.
- **WebLogic Server** - Komerční aplikační server od Oracle.

Komerční řešení se snaží poskytovat maximální výkon, stabilitu a podporu. Proto dokonce používají vlastní Java Virtual Machine. K některým open source serverům je možné dokoupit komerční podporu.

[remark]:<slide>(new)
## Servlet kontejner
Je běhové prostředí pracující jako webový server.

Je zodpovědný za správu životního cyklu servletů, mapování adresy URL na konkrétní servlet a zajištění toho, aby měl požadavek URL správných přístupových práv.

Zpracovává požadavky na servlety, soubory JavaServer Pages (JSP) a další typy souborů, které obsahují kód na straně serveru. 

Vytvoří instance servletu, načte a uvolní servlety, vytvoří a spravuje objekty požadavku a odezvy a provede další úlohy správy servletů.

Servlet kontejner implementuje contrakt architektury Java EE a specifikuje běhové prostředí pro webové komponenty, které zahrnuje zabezpečení, souběžnost, správu životního cyklu, transakce, nasazení a další služby.

Aplikační server **obsahje** servlet contejner.

Spring a Spring Boot nepouřívají Aplikační server, stačí jim pouze servlet container. 

[remark]:<slide>(new)
## Technologie v JavaEE
Nakonec si zmíníme některé technologie, které jsou v JEE obsažené.

- **JSP (Java Server Pages)** - Technologie umožňuje vkládat speciální direktivu do HTML kódu, která spustí Java kód. 
  Na daná místa ve stránce se tak vloží data, která získala Java např. z databáze.
- **JSF (Java Server Faces)** - Konkurenční a modernější technologie k JSP. 
  Celá webová stránka je reprezentována jako XML soubor. 
  Web se skládá z již připravených komponent (formuláře, tabulky, seznamy), které lze jednoduše plnit daty z Javy.
- **JDBC (Java DataBase Connectivity)** - JDBC a JPA známe již z našeho seriálu o JavaSE. 
  JDBC je standardní rozhraní pro práci s různými typy databází v jejich jazyce SQL.
- **JPA (Java Persistence API)** - JPA je rozhraní, umožňující objektovou práci s daty. 
  S databází nekomunikujeme přímo v SQL, ale pomocí mezivrstvy ORM. 
  Pracujeme tedy pouze s objekty. Konkrétní implementací je Hibernate.
- **EJB (Enterprise Java Beans)** - Komponenty obchodní logiky.
- **Spring framework** - Mezi nejznámější konkurenční řešení k EJB a JSF.

[remark]:<slide>(new)
#### Popularita servlet kontejnerů
![](media/Java_app_servers.png)

[remark]:<slide>(new)
### Definice servletu
Servlety jsou nástroj pro obsluhu protokolu HTTP na straně serveru. 

Přesně řečeno, jsou serverovým nástrojem pro obsluhu všech protokolů, které fungují způsobem požadavek-odpověď, nicméně už dlouhá léta je jediným podporovaným protokolem HTTP.
    
Servletem je každá Java třída, která implementuje interface `javax.servlet.Servlet`.
 
Protože ale z praktického hlediska má smysl uvažovat pouze servlety obsluhující protokol HTTP, je důležitější vědět, že HTTP servletem je každá třída, která je potomkem třídy `javax.servlet.http.HttpServlet`.
    
Servlety jsou základním kamenem, na kterém jsou vystavěny další vrstvy webových aplikací na platformě Java. 

Servlety jsou nízkoúrovňovým nástrojem pro obsluhu HTTP, takže na jednu stranu je možné s jejich pomocí obsloužit opravdu libovolný HTTP požadavek a vygenerovat libovolnou odpověď, na druhou stranu jsou pro rutinní generování HTML stránek příliš nepohodlné, a proto existují jejich nadstavby, zejména Java Server Pages.

[remark]:<slide>(new)
### Protokol HTTP
Pro pochopení servletů je důležité chápat, jak funguje protokol HTTP. 

Protokol HTTP (Hyper Text Transfer Protocol) je základním protokolem pro transport souborů a dat na webu.

Jde o jednoduchý textový protokol na aplikační vrstvě, fungující nad TCP/IP. 

Jeho použití má dvě fáze, požadavek klienta na server, bezprostředně následovaný odpovědí serveru v rámci stejného TCP spojení. 

Požadavek žádá o webový zdroj, jenž je adresován URL (Uniform Resource Identifier), které má pro protokol 

HTTP obecnou podobu:
```
http://user:password@machine:port/path?queryString#fragment
```

[remark]:<slide>(new)
### Protokol HTTP: URL
- **http** 
označuje protokol HTTP, případně může být tvaru https, pokud je přenos šifrován pomocí SSL
- **user:password** 
jméno a heslo uživatele, nedoporučuje se jej ale uvádět do URL kvůli bezpečnosti
- **port** 
číslo TCP portu, pokud není uvedeno, je to 80 pro http a 443 pro https
- **path** 
cesta nějakou hierarchií, např. souborovým systémem
- **queryString** 
doplňkové informace, typicky obsah polí z HTML formuláře
- **fragment** 
místo na stránce, je interpretován prohlížečem
Požadavek protokolem HTTP vypadá např. takto:

```
POST /mujservlet?p1=Pepa&p2=Mirek HTTP/1.1
Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5
Accept-language: cs,en-us;q=0.7,en;q=0.3
Connection: keep-alive
Host: www.cesnet.cz
Keep-Alive: 300
User-agent: Mozilla/5.0 (X11; U; Linux i686; cs-CZ; rv:1.8.1.6) Gecko/20070730 Firefox/2.0.0.6
Cookie: JSESSIONID=8823BD0993467256538E07E3FE10B883; style=meta;
Content-type: application/x-www-form-url-encoded
Content-length: 18 

p3=Lojza&p4=Franta
```

[remark]:<slide>(new)
### Protokol HTTP: Request
První řádek specifikuje tzv. metodu, která může být `GET`,` POST`, `PUT`, `HEAD`, `DELETE`, `OPTIONS`, `TRACE`, dále části path a queryString z URL, a verzi protokolu HTTP (1.0, 1.1, 2).

Další řádky až po prázdný řádek jsou tzv. hlavičky, obdobné MIME hlavičkám používaným v e-mailech. 

Prázdný řádek označuje konec hlaviček. U metody POST následuje za ním ještě datový obsah, jehož typ je určen hlavičkou `Content-type`. 

Ve výše uvedeném příkladu je to typ pro předávání dat z HTML formulářů, kdy jednotlivé položky formuláře jsou odděleny znakem ampersand, a mají tvar dvojice `jméno=hodnota`.

[remark]:<slide>(new)
### Protokol HTTP: Response
Odpověď serveru na HTTP požadavek vypadá obdobně:

```
HTTP/1.1 200 OK
Date: Tue, 09 Oct 2007 10:00:12 GMT
Server: Apache/2.2.6
Connection: close
Content-Type: text/html; charset=utf-8 

<html>
...
```

První řádek odpovědi určuje kód výsledku (200 OK, 301 Moved Permanently, ...), následují hlavičky, z nichž Content-type určuje typ obsahu za prázdným řádkem.

HTTP protokol je nezávislý na implementaci, tedy zda na straně serveru používáme Java Servlety, CGI programy, PHP, Perl, či něco jiného.

Verze HTTP 2 přináší multiplexing více HTTP requestů přes jediné TCP spojení, oproti HTTP 1.1/1.0 kde prohlížeče typicky otevírají 6 současných TCP spojení na jeden server.


[remark]:<slide>(new)
### Obsluha HTTP pomocí servletů
HTTP servlety implementují metodu service(). 

HTTP volání jsou reprezentovány vlákny, které volají metodu service(), a předávají jí dvojici parametrů reprezentujících HTTP požadavek a HTTP odpověď. 

Vlastní obsluha volání tak může být přímo v této metodě. Její standardní implementace však podle HTTP metody volá některou z metod doGet(), doPost(), doPut(), atd., do kterých je možné umístit obsluhu konkrétních HTTP metod.

[remark]:<slide>(new)
### Generování HTTP odpovědi
Minimalistický servlet, který generuje vždy stejný HTML text, je zde:

```java
import javax.servlet.http.*;
import javax.servlet.*;
import java.io.*;

public class Ukazka extends HttpServlet { 

    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) 
                      throws ServletException, IOException {
    
        response.setContentType("text/html; charset=utf-8");
        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("Hello, world !");
        out.println("</body></html>");
    }
}
 ```

[remark]:<slide>(wait) 
Tento servlet nijak nevyužívá informace z přicházejícího požadavku. 

Nejdříve nastaví typ obsahu na HTML text v kódování UTF-8, pak získá z objektu reprezentujícího odpověď PrintWriter, do kterého je možné text zapisovat, a text zapíše.

[remark]:<slide>(new)
## Webová aplikace a kontext servletu
Servlety jsou v rámci servletového kontejneru organizovány do tzv. webových aplikací, jeden kontejner může zároveň obsahovat více nezávislých webových aplikací. 

Jedna webová aplikace je tvořena jedním souborem s příponou .war, což je ZIP soubor obsahující servlety, další Java třídy, statické soubory (HTML,CSS,GIF,..) a JSP stránky.
 
Tento soubor je při spuštění aplikace obvykle rozbalen do stejnojmeného adresáře. Jeho adresářová struktura je:

- **`/WEB-INF/web.xml`**: soubor popisující webovou aplikaci
- **`/WEB-INF/classes/*/*.class`**: servlety a jiné Java třídy
- **`/WEB-INF/lib/*.jar`**: JAR balíky
- **`/*/*.jsp`**: JSP stránky
- **`/*/*.*`**: HTML, CSS, obrázky atd.

[remark]:<slide>(new)
## Mapování servletů na URL
Název webové aplikace (název WAR souboru a adresáře z něho vzniklého) je obvykle použit jako začátek cesty v URL odpovídajících této webové aplikaci.

[remark]:<slide>(new)
### Mapování ve web.xml
Soubor `/WEB-INF/web.xml` obsahuje definice mapování URL na servlety, plus další konfigurační údaje. 

Mapování URL na servlety se provádí buď podle začátku cesty v URL, nebo podle přípony. Následující příklad určuje, že třída MujServlet bude zavolána pro všechna URL začínající prefixem `/mujservlet/`, nebo končící příponou .muj:

```xml
<web-app ...
 ...
 <servlet>
     <servlet-name>mujServlet</servlet-name>
     <servlet-class>cz.nekde.mojeapp.MujServlet</servlet-class>
 </servlet>
 <servlet-mapping>
     <servlet-name>mujServlet</servlet-name>
     <url-pattern>/mujservlet/*</url-pattern>
 </servlet-mapping>
 <servlet-mapping>
     <servlet-name>mujServlet</servlet-name>
     <url-pattern>*.muj</url-pattern>
 </servlet-mapping>
```

[remark]:<slide>(new)
### Mapování po nasazení
Pokud by tento popisovač web.xml byl uvnitř souboru s webovou aplikací mojeapp.war, servlet MujServlet by byl vyvolán pro URL s cestou začínající /mojeapp/mujservlet/.

Jeden servlet (Java třída) může mít více instancí. Jedna instance může být mapována na různá URL.     

[remark]:<slide>(new)
### Anotace @WebServlet a web-fragment.xml
Od verze Servlet API 3.0 je možné místo do deployment descriptoru WEB-INF/web.xml uložit stejnou informaci na dvě další místa, do anotací

```java 
@WebServlet(name = "MujServlet",urlPatterns = {"/muj/*","*.muj"})
public class MujServlet extends HttpServlet {
    //...
}
```

nebo do souboru uvnitř některého JAR balíku, tj. `WEB-INF/lib/*.jar`.

[remark]:<slide>(new)
### Filtry
Kromě servletů může webová aplikace obsahovat ješte servletové filtry, což jsou Java třídy implementující interface `javax.servlet.Filter`, umožňující ovlivnit zpracování HTTP volání dřív, než se dostane k servletům. Do filtrů je vhodné například umístit nastavení kódování českých znaků v požadavku.