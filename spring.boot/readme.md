[remark]:<class>(center, middle)
# Spring Boot
## Rozdíl mezi Spring framework a Spring Boot

[remark]:<slide>(new)
## Spring framework je ...

Spring je lehký a otevřený framework vytvořený Rodem Johnsonem v roce 2003.
 
Spring je komplexní a modulární framewok a může být snadno použit pro implementaci všech vrsev aplikace.

Spring je velmi volný a invazivní, nenutí programátora implementovat vlastní třídy rošířením rozhraní nebo tríd ze Springku.

Spring vytužívá POJO třídy.
Díky Spring Framework je vývoj aplikací J2EE mnohem jednodušší

[remark]:<slide>(wait)
#### Spring má tyto základní výhody: 

- Jednoduchost
- Testability
- Volné spojení

[remark]:<slide>(new)
## Spring boot je ...

Spring Boot není framework.
 
Spring boot je způsob, jak snadno vytvořit samostatnou aplikaci s minimálními nebo nulovými konfiguracemi. 

Poskytuje výchozí nastavení konfigurace kódů a anotací pro rychlé spuštění nových projektů ve Spring frameworku. 

Spring Boot využívá stávajících Spring projektů, ale i projektů třetích stran, které integruje. 

Poskytuje soubor konfigurací **Starter** pro Maven nebo Gradl, které lze použít k přidání požadovaných závislostí a také k automatické konfiguraci.

Funkce Spring Boot automaticky konfiguruje požadované třídy v závislosti na knihovnách na své třídě cesty. 

[remark]:<slide>(new)
#### Příklad
Předpokládejme, že vaše aplikace chce komunikovat s databází DB.
- jestliže existují Spring Data library v projektu
- pak automaticky nastaví připojení k DB společně s třídou pro přístup k nim.


[remark]:<slide>(wait)
### Spring Boot má tyto základní výhody:     
- Je velmi snadné vyvinout aplikace napsané ve Spring frameworku
- Podporuje programovací jazyky: Java, Groovy, Kotlin.
- Spring Boot urchluje vývoj a zvyšuje produktivitu.
- Snižuje potřebné množství anotací a konfigurace XML.
- Velmi snadné je integrace Spring Boot Application s Spring JDBC, Spring ORM, Spring Data, Spring Security
- Nabízí zabudované HTTP servery jako Tomcat, Jetty atd.
- Poskytuje nástroj CLI (Command Line Interface), který umožňuje velmi snadno a rychle vyvíjet.
- Spring Boot poskytuje spoustu zásuvných modulů pro vývoj a testování pomocí nástrojů Maven a Gradle
- Poskytuje spoustu zásuvných modulů pro práci s vestavěnými a paměťovými databázemi.

[remark]:<slide>(new)
![](media/Configuration.png)