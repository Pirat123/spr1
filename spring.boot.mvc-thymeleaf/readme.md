[remark]:<class>(center, middle)
# Spring Boot
## Šablonovací systém Thymeleaf

[remark]:<slide>(new)
## Co je Thymeleaf?
Thymeleaf je moderní javovský šablonovací systém. 

Jedná se o open-source software vytvořený roku 2011 španělským inženýrem Danielem Fernándezem a v současnosti udržovaný týmem nadšenců.
 
Dobře si rozumí s frameworkem Spring kde jej lze snadno využít v pohledové vrstvě jako plnohodnotnou náhradu JSP (umožňují vkládání Java kódu do HTML). 

Mezi přednosti systému Thymeleaf patří elegantní syntaxe, konfigurovatelnost a extrémní rozšiřitelnost umožňující ovlivnit zpracování šablon do nejmenších detailů. 

Dalšími alternativami šablonovácích systémů pro framework Spring jsou například Apache Velocity nebo FreeMaker. 

Thymeleaf umožňuje zpracovávat 6 druhů šablon: HTML, XML, TEXT, JAVASCRIPT, CSS a RAW. 

Thymeleaf je aktuálně dostupný ve verzi 3.0.11.

[Dokumentace šablonovacího systému Thymeleaf](https://www.thymeleaf.org/doc/tutorials/3.0/usingthymeleaf.html)

[remark]:<slide>(new)
### Co Thymeleaf není?
Není to webový framework, je to pouze do něj integrovaná součást. 

Mnoho frameworků má svůj vlastní šablonovací systém. 

Šablonovací systém se stará o pohledovou vrstvu. 

Vytvořená šablona doplněná o data tvoří finální dokument, který je zobrazen uživateli. 

Příklad: `${user.name}` → Jan Novák 

[remark]:<slide>(new)
### Klíčové vlastnosti
- primárně určený pro Java webové aplikace

- využívá DOM

- lze jej použít na webu i pro offline dokumenty (email, XML data)

- vytváří XML, XHTML nebo HTML5

- vyhodnocování výrazů, internacionalizace, přepis URL

- konfigurovatelný a rozšiřitelný 

- možnosti statického prototypování - nový přístup

[remark]:<slide>(new)
## Natural templating
Tento princip je využíván systémem Thymeleaf. 

Podle něj musí být šablona stejně validní jako finální dokument. 

To znamená že syntaxe pro šablonovací engine nenarušuje strukturu dokumentu. 

Celá šablona je díky tomu staticky zobrazitelná - funguje jako designový prototyp. 

Těchto vlastností je dosaženo používáním vlastních atributů, které prohlížeče ignorují a vyvarováním se výrazů v tělech tagů. 

Na obrázcích níže je k vidění rozdíl ve statickém zobrazení dokumentu vytvořeného pomocí Thymeleaf oproti šabloně vytvořené pomocí JSP.

[remark]:<slide>(new)
### Natural templating - Thymeleaf
![alt text](https://www.thymeleaf.org/doc/articles/images/thvsjsp/th1.png)

[remark]:<slide>(new)
### Natural templating - JSP
![alt text](https://www.thymeleaf.org/doc/articles/images/thvsjsp/jsp3.png)

[remark]:<slide>(new)
## The Standard Dialect
Objekt, který přidává tagu nějakou logiku se nazývá processor. 

V Thymeleaf je většina processorů atributová. 

Skupina těchto processorů je pak nazývána jako dialect. 

Jádro systému Thymeleaf poskytuje tzv. Standard Dialect, který by měl svými funkcemi většině uživatelů zcela dostačovat. 

**Vybrané funkce:** 
- výrazy `${proměnná}` pro výpis proměnné a `*{proměnná}` pro zápis do ní
- přístup k beanám v aplikaci pomocí `${myBean.doSomething()}`
- Thymeleaf atributy pro formuláře: 
    * `th:field`
    * `th:errors`
    * `th:errorclass`
    * `th:object`

V případě pokročilých požadavků na integraci do Springu lze vytvořit novou instanci template enginu, která automaticky provede požadovanou konfiguraci. 

[remark]:<slide>(new)
### Základní konstrukty
Konstrukty Thymeleafu se přidávají principiálně přidáním atributů s prefixem `th:` do HTML tagů. 

Hodnoty z langtext souborů (pozn. langtext soubory umožňují uložit vkládaný text mimo šablonu) se vkládají pomocí `#{nazev.vlastnost}`, hodnoty z proměnných se vkládají pomocí `${nazevPromenne}.` 

[remark]:<slide>(wait)
#### Ukázka - Podmíněné zobrazení elementu s textem
```html
<div  th:if=”${hodnota == 100}” th:text=”${name}”>původní text</div> 
```

[remark]:<slide>(new)
### Příklad použití šablonovacího systému
- **Zavislosti**
  - do souboru `build.gradle` 
  - přidáme dependency `implementation('org.springframework.boot:spring-boot-starter-thymeleaf')`

[remark]:<slide>(wait)
- ve složce resources vytvoříme složky `static` a `templates`

[remark]:<slide>(wait)
- do složky templates vytvoříme soubor `greeting.html`

[remark]:<slide>(wait)
- vytvoříme package person a třídu `PersonController`

```java
@Controller  
public class PersonController {  

    @GetMapping("/")  
    public String welcome(Map<String, Object> model){  
        model.put("greeting", "Hello world!");  
        return "greeting";  
    }  
}
```

[remark]:<slide>(new)
### Příklad použití šablonovacího systému
- Do souboru šablony můžeme zatím použít tento kód

```html
<!DOCTYPE html>  
<html xmlns:th="http://www.thymeleaf.org"> 
<head>
    <meta charset="UTF-8">  
    <title>Welcome</title>  
    <link rel="stylesheet" 
          href="../../static/css/bootstrap.min.css"
          th:href="@{/css/bootstrap.css}"/>  
    <link rel="stylesheet" 
          href="../../static/css/main.css"  
          th:href="@{/css/main.css}"/>
</head>  
<body>  
    <h1 th:text="${greeting}"></h1>  
</body>  
</html> 

```
[remark]:<slide>(wait)
- Spustíme aplikaci

[remark]:<slide>(new)
### Příklad formuláře pro přidání osoby

-  Vytvoříme třídu Person

```java
@Data  
public class Person {  
        private String name;  
        private String surname;  
}
```

[remark]:<slide>(wait)
-  Vytvoříme potřebné metody v PersonControlleru

```java
@GetMapping("/form")  
public String form(Map<String, Object> model){  
    model.put("person", new Person());  
    return "form";  
}  

@PostMapping("/sendForm")  
public String formSubmit(
        @ModelAttribute Person person, 
        Map<String, Object> model
){  
    model.put("person", person);  
    return "result";  
}
```

[remark]:<slide>(new)
### Příklad formuláře pro přidání osoby
- Vytvoříme HTML stránku form.html

```html
<!DOCTYPE html>  
<html xmlns:th="http://www.thymeleaf.org"> 
<head>
    <!-- Header definition -->
</head> 
<body>  
    <div class="container">  
        <form method="post" 
              th:action="@{/sendForm}" 
              th:object="${person}">  
            <div class="form-group">  
                <label for="name">Jméno</label>  
                <input type="text" 
                       class="form-control" 
                       id="name" 
                       th:field="*{name}">  
                <label for="surname">Příjmení</label>  
                <input type="text" 
                       class="form-control" 
                       id="surname" 
                       th:field="*{surname}">  
            </div>  
            <button type="submit" class="btn btn-default">Odeslat</button>  
        </form>  
    </div>  
</body>  
</html>
```

[remark]:<slide>(new)
### Příklad formuláře pro přidání osoby
- Vytvoříme HTML stránku result.html

```html
<!DOCTYPE html>  
<html xmlns:th="http://www.thymeleaf.org"> 
<head>
    <!-- Header definition -->
</head> 
<body>  
    <div class="container">  
        <h1>Vloženo</h1>  
        <p th:text="${person.getName() + ' ' + person.getSurname()}"></p>   
        <a th:href="@{/form}">Zpět na formulář</a>  
    </div>  
</body>  
</html>
```

[remark]:<slide>(wait)
- Spustíme aplikaci 

[remark]:<slide>(new)
## Funkce pro tvorbu šablon

[remark]:<slide>(wait)
### Zápis textu
- `th:text="HTML escapovaný text (výchozí)"`
- `th:utext="neescapovaný text"`

[remark]:<slide>(wait)
### Formátování
`#dates`, `#calendars`, `#numbers`, `#strings`

a další ... (https://www.thymeleaf.org/doc/tutorials/3.0/usingthymeleaf.html)

#### Příklad: 

```html
<dd th:text="${#dates.format(product.availableFrom, 'dd-MM-yyyy')}">
    28-Jun-2018
</dd>
```

[remark]:<slide>(new)
### URL adresy
```html
<a th:href="@{/product.action(id=${product.id})}">View product</a>
```

vytvoří 

```html
<a href="/myAppContext/product.action?id=264">View product</a>
```

[remark]:<slide>(wait)
### Iterace
**Each:**

```html
<tr th:each="product : ${productList}">
    <td th:text="${productStat.count}">1</td>  <!-- číslo iterace -->         
    <td th:text="${product.description}">Red chair</td>
    <td th:text="${product.price}">350 €</td>
</tr>    
```

[remark]:<slide>(new)
### Podmínky
**If:**
```html
<span th:if="${product.price lt 100}">Special offer!</span> 
```

**Unless:**
```html
<span th:unless="${product.notInStock}">Currently in stock!</span>
```

**Switch/case:**
```html
<div th:switch="${user.role}">
    <p th:case="'admin'">User is an administrator</p>
    <p th:case="#{roles.manager}">User is a manager</p>
    <p th:case="*">User is something other</p>
</div>    
```

[remark]:<slide>(new)
### Formuláře a bean-binding
**th:object, th:field**

Atribut `th-object` může být v celém formuláři pouze jednou a neakceptuje navigaci k jednotlivým atributům
 
- je třeba uvádět vždy celou proměnnou z modelu. Tzn. nelze psát `th:object="${property.data}"` 
- je nutné vložit celou proměnnou `th:object="${property}"`.

Atribut `th-field` prováže hodnotu ve formuláři s proměnnou, je nutné zapisovat jako selektivní výraz `*{...}`.

[remark]:<slide>(new)
#### HTML Formuláře a bean-binding
```html
<form th:action="@{/saveCustomer}" 
      th:object="${customer}" 
      method="post">
    <input type="hidden" 
           th:field="*{id}" />
    
    <label for="name">Name:</label>
    <input type="text" 
           th:field="*{name}" />
    
    <label for="countryId">Country:</label>
    <select th:field="*{countryId}">
        <option th:each="country : ${allCountries}" 
                th:value="${country.id}" 
                th:text="${country.name}">Ireland</option>
    </select>
    
    <input type="submit" />
</form>
```

[remark]:<slide>(new)
**Validace - podmíněné stylování**

```html
<input type="text" 
       class="form-control" 
       id="message" 
       placeholder="enter text ..." 
       th:field="*{message}" 
       th:class="${#fields.hasErrors('message')}? fieldError"> 
```

Poznámka: V tomto případě lze s výhodou využít atribut `th:errorclass="fieldError"`

[remark]:<slide>(wait)
**Iterace přes chyby**
```html
<ul>
    <li th:each="err : ${#fields.errors('message')}" 
        th:text="${err}" />
</ul> 
```

[remark]:<slide>(new)
### Kompozice stránky
**Deklarace fragmentu pomocí `th:fragment`**

```html
<h2>Product information</h2>
<div th:fragment="banner">
    <img th:src="@{/logo.png}" 
         alt="Some logo" />
</div>    
``` 

[remark]:<slide>(wait)
**Znovupoužití fragmentu pomocí `th:include`**

Při znovupoužití se vloží obsah mezi značkami, na kterých byl tento atribut definován.

```html
<h2>Product list</h2>
<div th:include="index.html::banner">Banner</div>    
```

[remark]:<slide>(new)
### Renderování fragmentů šablony
Thymeleaf nabízí možnost jako odpověď vygenerovat pouze fragment z pohledu. 

Lze to využít například pro přednostní zobrazení načtených komponent, zatímco ostatní se ještě načítají. 

Specifikace lze provést pomocí view-beans či přes návratovou hodnotu controlleru.

```java
@RequestMapping("/menu")
public String menu(Map<String, Object> model) {
    return "index :: menu";  //show only menu fragment
}
```

[remark]:<slide>(new)
### Inlining
Ačkoli je téměř vše možné vyřešit pomocí atributů tagů, někdy se může hodit vkládat výrazy přímo do HTML textu.

Preferujeme 

```html
<p>Hello, [[${session.user.name}]]!</p>
```
 
před 

```html
<p>Hello, <span th:text="${session.user.name}">Sebastian</span>!</p> 
```

[remark]:<slide>(new)
## Spring security
Následující část předpokládá, že máme Spring Boot aplikaci s funkčním **Thymeleaf** a **Spring security**.

[remark]:<slide>(wait)
### Přihlašovací stránka
Konfigurace **Spring Security**

```java
@Override
protected void configure(final HttpSecurity http) throws Exception {
    http
        .formLogin()
        .loginPage("/login.html")
        .failureUrl("/login-error.html")
      .and()
        .logout()
        .logoutSuccessUrl("/index.html");
}
```

[remark]:<slide>(new)
Vytvoření **Spring Controlleru**

```java
@Controller
public class SecurityController {

  // Login form
  @RequestMapping("/login.html")
  public String login() {
    return "login";
  }

  // Login form with error
  @RequestMapping("/login-error.html")
  public String loginError(Model model) {
    model.addAttribute("loginError", true);
    return "login";
  }

}
```

[remark]:<slide>(new)
Vytvoření **Thymeleaf template** `login.html`

```html
<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
  <head>
    <title>Login page</title>
  </head>
  <body>
    <h1>Login page</h1>
    <p th:if="${loginError}" 
       class="error">Wrong user or password</p>
    <form th:action="@{/login.html}" 
          method="post">
      <label for="username">Username</label>:
      <input type="text" 
             id="username" 
             name="username" 
             autofocus="autofocus" /> <br />
      <label for="password">Password</label>:
      <input type="password" 
             id="password" 
             name="password" /> <br />
      <input type="submit" 
             value="Log in" />
    </form>
  </body>
</html>
```

[remark]:<slide>(new)
### Chybová stránka
Vytvoření **Error Controller**
```java
@ControllerAdvice
public class ErrorController {

    private static Logger logger = LoggerFactory.getLogger(ErrorController.class);

    @ExceptionHandler(Throwable.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public String exception(final Throwable throwable, final Model model) {
        logger.error(
                "Exception during execution of SpringSecurity application", 
                throwable);
        String errorMessage = (throwable != null) 
                ? throwable.getMessage() 
                : "Unknown error";
        model.addAttribute(
                "errorMessage", 
                errorMessage);
        return "error";
    }

}

```

[remark]:<slide>(new)
Vytvoření **Thymeleaf template** `error.html`

```html
<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
    <head>
        <title>Error page</title>
        <meta charset="utf-8" />
        <link rel="stylesheet" 
              href="css/main.css" 
              th:href="@{/css/main.css}" />
    </head>
    <body th:with="httpStatus=${T(HttpStatus).valueOf(#response.status)}">
        <h1 th:text="|${httpStatus} - ${httpStatus.reasonPhrase}|">404</h1>
        <p th:utext="${errorMessage}">Error java.lang.NullPointerException</p>
        <a href="index.html" th:href="@{/index.html}">Back to Home Page</a>
    </body>
</html>
```

[remark]:<slide>(new)
### Security dialekt
Pro použítí `sec:` namespace je potřeba načíst [integrační modul](https://github.com/thymeleaf/thymeleaf-extras-springsecurity)

Použití **sec:authorize** v šabloně:

```html
<div sec:authorize="isAuthenticated()">
  This content is only shown to authenticated users.
</div>
<div sec:authorize="hasRole('ROLE_ADMIN')">
  This content is only shown to administrators.
</div>
<div sec:authorize="hasRole('ROLE_USER')">
  This content is only shown to users.
</div>
```

Získání informací o uživateli pomocí **sec:authentication**
 
```html
Logged user: <span sec:authentication="name">Bob</span>
Roles: <span sec:authentication="principal.authorities">[ROLES]</span>
```