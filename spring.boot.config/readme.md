[remark]:<class>(center, middle)
# Spring Boot
## Konfigurace aplikace

[remark]:<slide>(new)
## Základy konfigurace
Funkce Spring Boot podporuje:
- Obdsahuje sadu vlastností na konfiguraci
- Umožňuje vytvažet vlastí konfiurační hodnoty
- Podporuje tvorbu konfiguračních profilů
- V konfiguracích je možné používat expresion language

Kongiruca se prování:
- pomocí souborů `.properties`
- pomoci souborů `.yaml`
- proměnné prostředí
- argumenty příkazového řádku pro externí konfiguraci. 

Hodnoty vlastností lze vkládat přímo do vašich bean pomocí anotace @Value, 

Spring Boot používá velmi specifické uspořádání `PropertySource`, které je navrženo tak, aby umožňovalo rozumné přetěžování hodnot. 

[remark]:<slide>(new)
## Priority konfigurace
Vlastnosti jsou zvažovány v následujícím pořadí:
- Konfigurace testů:
  * `@TestPropertySource`
  * `@SpringBootTest`
- Konfigurace při spuštění
  * Argumenty příkazového řádku.
  * OS proměnné prostředí.
  * RandomValuePropertySource, který má pouze náhodné vlastnosti.
- Konfigurace v souborech `application.properties` a `application.yaml`
  * Možnost definovat profily: `application-{profile}.properties`
  * Konfigurace mimo jar soubory má přednost
- Výchozí vlastnosti (určuje nastavení SpringApplication.setDefaultProperties).

[remark]:<slide>(new)
### Příklad použití konfigurace
V komponentě @Component, chceme použít vlastnost `name`:

```java
import org.springframework.stereotype.*;
import org.springframework.beans.factory.annotation.*;

@Component
public class MyBean {

    @Value("${name}")
    private String name;

    // ...

}
```

[remark]:<slide>(wait)
`name` pak můžeme nastavit třeba z příkazového řádku

```
java -jar app.jar --name="Spring"
```

nebo v property souboru

```properties
name=Spring
```

[remark]:<slide>(new)
## Náhodné hodnoty
Hodí se pro testová, pro generován bezpečnostních klíčů, ...

```properties
my.secret=${random.value}
my.number=${random.int}
my.bignumber=${random.long}
my.uuid=${random.uuid}
my.number.less.than.ten=${random.int(10)}
my.number.in.range=${random.int[1024,65536]}
```

[remark]:<slide>(wait)
## Placeholders v properties
Propery je možné mezi sebou provazovat:

```properties
app.name=MyApp
app.description=${app.name} is a Spring Boot application
```

[remark]:<slide>(new)
## Konfigurašní soubory `application`
Spring Boot je hledá soubory s názvem:
- `application.properties` a `application.yaml`
- `applicatiom-{profile}.properties` a `application-{profile}.yaml`

Místa kde se hledají:
1. Ve složce `/config` aktuálního pracovního adresáře
2. V aktuálním pracovním adresáři
3. A adreáří `/config` v classpath
4. V kořenovém adresáři classpath

Je možné je určit další místa a konkrétní názvy souborů.

[remark]:<slide>(new)
## Používejte YAML misto properties
[YAML](https://yaml.org/) je textový formát vhodná pro zápis hirearchicky strukturovaných dat.

Jeho výhody si ukážeme na příkladech

[remark]:<slide>(wait)
#### Porovnejte:

```properties
environments.dev.url=http://dev.example.com
environments.dev.name=Developer Setup
environments.prod.url=http://another.example.com
environments.prod.name=My Cool App
```

```yaml
environments:
	dev:
		url: http://dev.example.com
		name: Developer Setup
	prod:
		url: http://another.example.com
		name: My Cool App
```

[remark]:<slide>(new)
### Používání listů
Yaml narozdíl od properites podporuje i seznamy.

```properties
my.servers[0]=dev.example.com
my.servers[1]=another.example.com
```

misto:
```yaml
my.servers:
	- dev.example.com
	- another.example.com
```

A možnost napojení do Javy:
```java
@ConfigurationProperties(prefix="my")
public class Config {

	private List<String> servers = new ArrayList<String>();

	public List<String> getServers() {
		return this.servers;
	}
}
```
[remark]:<slide>(new)
### Yaml a profily
Yaml umožňuje definovat více profilů v jednom souboru.

```yaml
server:
	address: 192.168.1.100
---
spring:
	profiles: development
server:
	address: 127.0.0.1
---
spring:
	profiles: production & eu-central
server:
	address: 192.168.1.120
```

[remark]:<slide>(new)
## Profily
Jak definovat profily v `application.properties` a `yaml` již víme.

Jak spojit nějkou kongigurační třídu s profilem:
```java
@Configuration
@Profile("production")
public class ProductionConfiguration {

	// ...

}
```

[remark]:<slide>(new)
## Nastavení aktivních profilů
Způsoby jak říci, které profily se použijí:

#### V property souborech:
```properties
spring.profiles.active=dev,hsqldb
```

#### V příkazové řádce
```cmd
--spring.profiles.active=dev,hsqldb
```

[remark]:<slide>(new)
## Výchozí propery a jejich hodnot:
Kompletní seznam je v příloze dokumentace: [zde](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#common-application-properties)

**A to je vše?**

[remark]:<slide>(wait)
## Centrální konfigurace
Spring boot podporuje i konfiguraci prostřednictvím centrálního konfiguračního serveru.

To má tu výhodu. že můžete například měnit konfigurace bez restartu klienta.

Doporučuji si vyzkoušet příklad: [Centralized Configuration](https://spring.io/guides/gs/centralized-configuration/)



