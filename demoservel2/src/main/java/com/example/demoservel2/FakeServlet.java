package com.example.demoservel2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.MessageFormat;

@Controller
public class FakeServlet {

    private static final Logger log = LoggerFactory
            .getLogger(FakeServlet.class);

    @GetMapping("/fake")
    public void doGet(HttpServletRequest req,
                      HttpServletResponse res)
            throws IOException {
        log.info(req.toString());
        log.info("params: {}", req.getParameterNames());
        res.setStatus(400);
        res.sendRedirect("http://ictpro.cz");
        res.getWriter().println("Hello world");
    }

    @GetMapping("/hello")
    public @ResponseBody
    String hello(
            @RequestParam(defaultValue = "world") String name) {
        log.info("Hello request for {}.", name);
        return String.format("Hello %s!", name);
    }

    @GetMapping("/hello/{name}")
    public @ResponseBody
    String helloName(
            @PathVariable String name) {
        log.info("Hello request for {}.", name);
        return String.format("Hello %s!", name);
    }

}
