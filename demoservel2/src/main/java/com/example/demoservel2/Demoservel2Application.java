package com.example.demoservel2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demoservel2Application {

    public static void main(String[] args) {
        SpringApplication.run(Demoservel2Application.class, args);
    }

}
