[remark]:<class>(center, middle)
# Spring Boot
*REST Service*

[remark]:<slide>(new)
## Úvod

V rámci tohoto příkladu vytvoříme "Hello World" RESTful webovou službu.

Více informací o [REST](https://spring.io/understanding/REST)

[remark]:<slide>(wait)
### Zadání
Služba bude přijímat `HTTP GET` na adrese

```
http://localhost:8080/greeting
```

a vygeneruje zprávu v JSON formátu:

```json
{"id":1,"content":"Hello, World!"}
```

Pozdrav budé možné paralelizovat pomocí parametru `name`

```
http://localhost:8080/greeting?name=User
```

Hodnota parametru `name` modifikuje odpovď nahrazením za "Svět":

```json
{"id":1,"content":"Hello, User!"}
```

[remark]:<slide>(new)
## Gradle konfigurace
Pro konfiguraci projektu použijem [Gradle](https://gradle.org/) script. 

Podobně je ale možno použít i [Maven](https://maven.apache.org/).

Ve sriptu použijeme následující `dependencies`

#### `build.gradle`
```groovy
dependencies {
    compile("org.springframework.boot:spring-boot-starter-web")
}
```


[remark]:<slide>(new)
## Vytvořte třídu pro resource
Pro vytvoření odpovědi ve formátu `JSON` je použita [Jackson JSON](http://jackson.codehaus.org/Home).

Odpovědi ale i požadavky se definují **POJO** objektem.

#### `src/main/java/hello/Greeting.java`
```java
package hello;

public class Greeting {

    private final long id;
    private final String content;

    public Greeting(long id, String content) {
        this.id = id;
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }
}
```

[remark]:<slide>(new)
## Vytvořte regulátor prostředků
Pro zpravocání požadavků Vytvoříme třídu `GreetingController`.

Ta musí být označena annotací `@RestController`.

#### `src/main/java/hello/GreetingController.java`
```java
package hello;

import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.*;

@RestController
public class GreetingController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @GetMappaping("/greeting")
    public Greeting greeting(
            @RequestParam(value="name", defaultValue="World") String name) {
        return new Greeting(counter.incrementAndGet(),
                            String.format(template, name));
    }
}
```

[remark]:<slide>(new)
### Rozbor třídy `GreetingController`

Metoda `greeting` označená anotací `@GetMapping` zajišťuje zpracování HTTP požadavku `/greeting`
 
[remark]:<slide>(wait) 
Annotace `@RequestParam` říká, že jako hodnota parametru `name` bude nastavená z url.
- Pokud url nemá parametr `name` je použita výchozí hodnota `defaultValue=`.

[remark]:<slide>(wait) 
Implementace metody vytváří objekt pozdravný objekt `Greeting` s atributy:
- **id** generované pomocí čítače `counter`
- **content** vytvořená z parametru `name` a šablony `template`

[remark]:<slide>(wait) 
Metoda vrací data přímo, nepoužívá šablonu pro jejich transformaci do HTML.

[remark]:<slide>(wait) 
Anotace `@RestController` je zkratka pro `@Controller` a `@ResponseBody`

[remark]:<slide>(wait) 
Objekt `Greeting` musí být převeden do formátu JSON. 
- Spring provádí konverzi automaticky. 
- Používá k tomu Jackson 2 a beanu `MappingJackson2HttpMessageConverter`

[remark]:<slide>(new)
## Vytvoření spustitelné aplikace
Spring Boot umožňuje vytvořit tradiční soubor WAR.
 
Jednodušší přístup je ale vytvoření samostatné aplikace. 

Balíte vše v jediném spustitelném souboru JAR, spustileném díky metody `main`. 

Spring Boot integruje *Tomcat* jako runtime HTTP do naší aplikace.

#### `src/main/java/hello/Application.java`
```java
package hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
```

[remark]:<slide>(new)
## Rozbor třídy `Application`

`@SpringBootApplication` se zkládá z:
- `@Configuration` označuje třídu jako zdroj konfigurace pro Spring.
- `@EnableAutoConfiguration`,
- `@EnableWebMvc` pro aplikaci Spring MVC
- `@ComponentScan` říká kde se mají hledat dalčí součásti aplikace

Metoda `main()` používá metodu `SpringApplication.run()` k spuštění aplikace. 

Celá aplikace je bez XML konfigurace (například žádný soubor `web.xml`), díky tomu se nemusíme zabývat žádnou konfirgurací aplikace ani infrastruktury.

[remark]:<slide>(new)
## Sestavení a spuštění

Vytvoření spustitelného JAR souboru pomocí programu Gradle nebo Maven. 

Nebo můžete vytvořit jeden spustitelný soubor JAR, který obsahuje všechny potřebné závislosti, třídy a zdroje, a spusťte jej. 

To usnadňuje odesílání, verzi a nasazení služby jako aplikace po celou dobu vývoje, v různých prostředích a podobně.

Pokud používáte Gradle, můžete spustit aplikaci pomocí `./gradlew bootRun`

[remark]:<slide>(new)
## Testování aplikace
Po zadání url `http://localhost:8080/greeting` do prohlížeče, měli by jste získat nasleující výtstup:

```json
{"id":1,"content":"Hello, World!"}
```

Pokud do uml nastavíme paramter `name`: `http://localhost:8080/greeting?name=User`, odpověď se změní následovně:

```json
{"id":2,"content":"Hello, User!"}
```

Díky inkrementaci coutnru má každá odpověď jedinečné `id`.

[remark]:<slide>(new)
## Rozšíření:
V REST není dobré odesílat objekt přímo, je jepší jej "zabalit" a přidat k nemu metadata, například čas vytvoření.
- Upravte odpověď títo způsobem

Složitější data není vhodné posílat jako parametry, je lepší je odeslat v těle.
- Upravte příklad tak, aby jméno bylo posláno jako JSON object.