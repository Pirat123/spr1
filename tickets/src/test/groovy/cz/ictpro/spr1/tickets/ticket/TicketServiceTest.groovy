package cz.ictpro.spr1.tickets.ticket

import spock.lang.Specification
import spock.lang.Unroll

class TicketServiceTest extends Specification {

    private TicketService ticketService;

    private TicketRepository ticketRepository;

    void setup() {
        ticketService = new TicketService();
        ticketService.ticketRepository = ticketRepository = Mock(TicketRepository);
    }

    def "Create Ticket by Type and Description."() {
        given: "Mam typ a popis"
        Type type = Type.IN;
        String desc = "Popis";
        ticketRepository.create(_ as Ticket) >> { Ticket t -> Optional.of(t) }

        when: "Vytvorim na service metodou ...."
        Ticket result = ticketService.createTicket(type, desc);

        then: "Testuji ze se neco vrati"
        result != null;

        and: "ze to je dobre nastaven0"
        result.getDescription() == desc;
        result.getType() == type;
        result.getStatus() == Status.NEW;
    }

    def "Ticket comare test"() {
        given:
        Ticket ticket = new Ticket(
                id: UUID.randomUUID(),
                type: Type.SR);
    }

    @Unroll
    def "Plus Demo test #a + #b = #c."() {
        expect:
        a + b == c

        where:
        a | b | c
        1 | 2 | 3
        5 | 4 | 9
        4 | 5 | 9
    }
}
