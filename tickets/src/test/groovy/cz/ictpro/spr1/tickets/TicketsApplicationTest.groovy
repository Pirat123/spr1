package cz.ictpro.spr1.tickets

import spock.lang.Specification

class TicketsApplicationTest extends Specification {

    def "Url content length"() {
        given:
        def urlContentSize = new TicketsApplication.UrlContentSize("https://www.seznam.cz/");
        when:
        def length = urlContentSize.call();
        then:
        length > 40000 ;
    }
}
