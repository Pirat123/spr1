package cz.ictpro.spr1.tickets.ticket;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;
import java.util.UUID;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
public class TicketServiceSpringTest {

    @TestConfiguration
    static class TicketServiceTestConfiguration {

        @Bean
        public TicketService ticketService() {
            return new TicketService();
        }

        @Bean
        public TicketRepository ticketRepository() {
            return new TicketRepository() {
                @Override
                public Optional<Ticket> getById(UUID uuid) {
                    // TODO Test
                    return super.getById(uuid);
                }

                @Override
                public Optional<Ticket> update(Ticket ticket) {
                    // TODO Test
                    return super.update(ticket);
                }

                @Override
                public Optional<Ticket> create(Ticket ticket) {
                    return Optional.of(ticket);
                }
            };
        }
    }

    @Autowired
    private TicketService ticketService;

//    @Test
//    public void createTicket() {
//        Ticket ticket = ticketService.createTicket(Type.IN, "Popis");
//        assertNotNull(ticket);
//        // assert ...
//    }
}