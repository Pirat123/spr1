package cz.ictpro.spr1.tickets.person;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import java.util.UUID;

/**
 * Person data object.
 */
@Data
@Entity
public class Person {

    @Id
    @Type(type = "uuid-char")
    @JsonIgnore
    private UUID id ;

    @Column(unique = true)
    private String email;

    private String name;
}
