package cz.ictpro.spr1.tickets.person;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.util.UUID;

@RestController
@RequestMapping("/api")
public class PersonController {

    @Autowired
    private PersonService personService;

    @Data static class CreatePersonRequest {
        @Pattern(regexp = ".*@.*")
        private String email;
        @NotEmpty
        private String name;
    }

    @PostMapping("/person")
    @ResponseStatus(HttpStatus.CREATED)
    public Person createPerson(
            @RequestBody @Valid CreatePersonRequest cpr
    ) {
        return personService.createPerson(
                cpr.getEmail(),
                cpr.getName()
        );
    }

    @GetMapping("/person")
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Person> getAllByName(
            @RequestParam String name
    ) {
        return personService.findAllByName(name);
    }

    @GetMapping("/person/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Person getById(
            @PathVariable UUID id
    ) {
        return personService.findById(id);
    }

}
