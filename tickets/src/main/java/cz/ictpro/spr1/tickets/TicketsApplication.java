package cz.ictpro.spr1.tickets;

import cz.ictpro.spr1.tickets.person.Person;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.client.RestTemplate;

import java.awt.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;
import java.util.function.Function;

@Slf4j
@EnableScheduling
@SpringBootApplication
public class TicketsApplication {

    public static void main(String[] args) {
        SpringApplication.run(TicketsApplication.class, args);
    }

    @Data
    public static class PersonResponse {
        private List<Person> content;
        private Integer count;
    }

    //@Scheduled(fixedRate = 3000)
    public void updatePerson() {
        log.info("Time is {}", LocalDateTime.now());
        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<PersonResponse> response = restTemplate.getForEntity(
                        "http://localhost:8081/api/person",
                        PersonResponse.class);
        log.info(response.getBody().toString());
    }

    private final ExecutorService executorService = Executors.newWorkStealingPool();

    public static class UrlContentSize implements Callable<Integer> {

        private final URL url;

        public UrlContentSize(String url) {
            try {
                this.url = new URL(url);
            } catch (MalformedURLException e) {
                throw new IllegalArgumentException("Bad url: " + url);
            }
        }

        @Override
        public Integer call() throws Exception {
            try {
                log.info("Start {} in thread {}", url, Thread.currentThread().getName());
                Thread.sleep(500);
                return url.openStream().readAllBytes().length;
            } finally {
                log.info("Done url {}", url);
            }
        }
    }

    @Bean
    public CommandLineRunner runner() {
        return args -> {
            List<UrlContentSize> tasks = Arrays.asList(
                    new UrlContentSize("https://seznam.cz/"),
                    new UrlContentSize("https://www.ictpro.cz/"),
                    new UrlContentSize("http://mendelu.cz/"),
                    new UrlContentSize("https://www.nytimes.com/"),
                    new UrlContentSize("https://www.zatrolene-hry.cz/"),
                    new UrlContentSize("http://www.glockshop.cz/")
            );

            List<Future<Integer>> futures = executorService.invokeAll(tasks);
            log.info("Tasks send");
            int sum = futures.stream()
                    .mapToInt(this::futureGet)
                    //.peek(System.out::println)
                    .sum();
            log.info("Suma dat: {}", sum);


            log.info("First {}", executorService.invokeAny(tasks));

            Future<Integer> future = executorService.submit(tasks.get(0));
            log.info("Is future done {}", future.isDone());
            log.info("Result {}", futureGet(future));
        };
    }

    public int futureGet(Future<Integer> future) {
        try {
            return future.get();
        } catch (Exception e) {
            log.error("future get failed: ", e);
            return 0;
        }
    }


}
