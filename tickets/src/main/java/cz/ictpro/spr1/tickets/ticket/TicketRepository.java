package cz.ictpro.spr1.tickets.ticket;

import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

@Component
public class TicketRepository {

    /**
     * Find ticket by ID.
     * @param uuid not null
     * @return search result.
     */
    public Optional<Ticket> getById(UUID uuid) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    /**
     * Update existing ticket in database.
     * @param ticket not nul existing ticket.
     * @return actual state of ticket.
     */
    public Optional<Ticket> update(Ticket ticket) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    /**
     * Create new record in database.
     * @param ticket not nul existing ticket.
     * @return actual state of ticket.
     */
    public Optional<Ticket> create(Ticket ticket) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }
}
