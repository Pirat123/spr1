package cz.ictpro.spr1.tickets.ticket;

import cz.ictpro.spr1.tickets.person.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.UUID;

@Service
public class TicketService {

    @Autowired
    private TicketRepository ticketRepository;

    /**
     * Create new ticket.
     */
    public Ticket createTicket(Type type, String description) {
        Ticket ticket = new Ticket();
        ticket.setId(UUID.randomUUID());
        ticket.setStatus(Status.NEW);
        ticket.setType(type);
        ticket.setDescription(description);
        ticket.setDateTime(LocalDateTime.now());

        return ticketRepository
                .create(ticket)
                .orElse(null);
    }
}
