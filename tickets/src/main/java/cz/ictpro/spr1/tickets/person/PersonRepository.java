package cz.ictpro.spr1.tickets.person;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface PersonRepository extends PagingAndSortingRepository<Person, UUID> {

    /**
     * Najde vsechny osoby stejneho jmena
     * @param name
     * @return
     */
    @Query("select p from Person as p where p.name like :name")
    Iterable<Person> findAllByName(@Param("name") String name);
}
