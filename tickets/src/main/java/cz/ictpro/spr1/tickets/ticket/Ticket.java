package cz.ictpro.spr1.tickets.ticket;

import cz.ictpro.spr1.tickets.person.Person;
import cz.ictpro.spr1.tickets.ticket.attachment.Attachment;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.UUID;

/**
 * Ticket data object.
 */
@Data
public class Ticket {

    private UUID id;
    private Person owner;
    private Type type;
    private Status status;
    private LocalDateTime dateTime;
    private String description;
    private Collection<Attachment> collections;

}
