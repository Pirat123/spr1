package cz.ictpro.spr1.tickets.ticket.attachment;

import lombok.Data;

import java.util.UUID;

/**
 * Attachment data object.
 */
@Data
public class Attachment {

    private UUID id;
    private String name;
    private byte[] data;
}
