package cz.ictpro.spr1.tickets.ticket;

/**
 * Tickets status.
 */
public enum Status {

    /** New Ticket status */
    NEW,
    /** Work in progress status. */
    WIP,
    /** Ticket closed status. */
    CLOSED

}
