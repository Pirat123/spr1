package cz.ictpro.spr1.tickets.person;

import cz.ictpro.spr1.tickets.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
public class PersonService {

    @Autowired
    private PersonRepository personRepository;

    @PreAuthorize("hasRole('ADMIN')")
    public Person createPerson(
            String email,
            String name) {
        Person person = new Person();
        person.setId(UUID.randomUUID());
        person.setEmail(email);
        person.setName(name);
        return personRepository.save(person);
    }

    @PreAuthorize("hasRole('USER')")
    public Iterable<Person> findAllByName(String name) {
        return personRepository.findAllByName(name);
    }

    @PreAuthorize("hasRole('ADMIN')")
    public Person findById(UUID id) {
        return personRepository
                .findById(id)
                .orElseThrow(NotFoundException::new);
    }
}
