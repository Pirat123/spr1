package cz.ictpro.spr1.tickets.ticket;

/**
 * Tickets types.
 */
public enum Type {

    /** Incident ticket type. */
    IN,
    /** Service request ticket type. */
    SR
}
