package com.example.demomvc.person;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class DuplicatePostException extends RuntimeException {

    private Person person;

    public DuplicatePostException(Person person) {
        super("Create duplicate entity.");
        this.person = person;
    }

    public Person getPerson() {
        return person;
    }
}
