package com.example.demomvc.person;

import com.example.demomvc.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/api")
public class PersonRestController {

    @Autowired
    private PersonService personService;

    @GetMapping("/person")
    @ResponseStatus(HttpStatus.OK)
    public RestResponse<Collection<Person>> getPersons() {
        Collection<Person> persons = personService.getPersons();
        return new RestResponse<>(persons, persons.size());
    }

    @PostMapping("/person")
    //@ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Person> postPerson(
            @RequestBody Person person) {


        boolean success = personService.addPerson(person);

        if (!success) {
            return new ResponseEntity<>(person, HttpStatus.CONFLICT);
        }

        return new ResponseEntity<>(person, HttpStatus.CREATED);
    }

    @PostMapping(
            path = "/person/create",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseStatus(HttpStatus.CREATED)
    public RestResponse<Person> postPersonCreate(
            @RequestBody Person person) {

        boolean success = personService.addPerson(person);

        if (!success) {
            throw new DuplicatePostException(person);
        }

        return new RestResponse<>(person, 1);
    }
}
