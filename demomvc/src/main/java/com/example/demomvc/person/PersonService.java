package com.example.demomvc.person;

import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class PersonService {

    private Set<Person> persons = new HashSet<>();

    public boolean addPerson(Person person) {
        return persons.add(person);
    }

    public Set<Person> getPersons() {
        return persons;
    }
}
