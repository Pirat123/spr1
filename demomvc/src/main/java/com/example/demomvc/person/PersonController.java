package com.example.demomvc.person;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/person")
public class PersonController {

    @Autowired
    private PersonService personService;

    @GetMapping
    public String index(Model model) {
        model.addAttribute("persons", personService.getPersons());
        return "person/list";
    }

    @GetMapping("/form")
    public String form(Model model) {
        Person newPerson = new Person();
        newPerson.setName("Karel");
        model.addAttribute(newPerson);
        return "person/form";
    }

    @PostMapping("/create")
    public String create(
            @ModelAttribute Person person,
            Model model) {
        model.addAttribute("created", personService.addPerson(person));
        model.addAttribute("persons", personService.getPersons());
        return "person/list";
    }

}
