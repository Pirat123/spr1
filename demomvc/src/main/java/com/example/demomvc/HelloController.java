package com.example.demomvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HelloController {

    @GetMapping("/hello")
    public String hello(
        @RequestParam(defaultValue = "World") String name,
        Model model
    ) {
        String message = String.format("Hello %s!", name);
        model.addAttribute("greeting", message);
        return "viewTemplateGreeting";
    }
}
