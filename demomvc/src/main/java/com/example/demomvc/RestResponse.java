package com.example.demomvc;

public class RestResponse<T> {

    private T content;

    private Integer count;

    public RestResponse(T content, Integer count) {
        this.content = content;
        this.count = count;
    }

    public T getContent() {
        return content;
    }

    public void setContent(T content) {
        this.content = content;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
