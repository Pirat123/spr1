package com.example.demo2;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicLong;

@Component
@Scope("prototype")
public class A {

    private final static AtomicLong nextId = new AtomicLong();

    private final long id;

    public A() {
        this.id = nextId.incrementAndGet();
    }


    @Override
    public String toString() {
        return "A{" +
                "id=" + id +
                '}';
    }
}
